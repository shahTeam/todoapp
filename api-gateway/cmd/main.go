package main

import (
	"api-gateway/handler"
	"api-gateway/services"
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"net/http"
)

const (
	userServiceGRPCURL = "localhost:8002"
	todoServiceGRPCURL = "localhost:8001"
)

func main() {
	h := handler.New(services.NewServices(userServiceGRPCURL, todoServiceGRPCURL))

	r := chi.NewRouter()

	r.Use(middleware.Logger)

	r.Post("/user/{name}", h.RegisterUser)               
	r.Get("/user/{user-id}", h.GetUser)                  
	r.Put("/user/update/{user-id}/{name}", h.UpdateUser) 

	r.Put("/todo/update", h.UpdateTodo) 
	r.Post("/todo", h.RegisterTodo)     
	r.Get("/todo/{todo-id}", h.GetTodo) 
	r.Get("/user/{user-id}/todos", h.GetUserTodos)

	fmt.Println("Listening at :8080")
	http.ListenAndServe(":8080", r)
}
