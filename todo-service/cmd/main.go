package main

import (
	"log"
	"net"
	"todo-service/config"
	"todo-service/grpc_server"
	"todo-service/postgres"
	"todo-service/psql"
	"todo-service/service"
	"todo-service/todopb"

	"github.com/todo/todo-service/connect"
	"google.golang.org/grpc"
)

func main() {
	cfg := config.Load()
	db, er := connect.Connect(cfg)
	if er != nil {
		log.Fatalf("error with connectDb: %v", er)
	}
	repo := psql.New(db)
	svc := service.New(repo)

	lis, er := net.Listen("tcp", "localhost:8001")
	if er != nil {
		log.Fatalf("%v", er)
	}

	s := grpc.NewServer()
	todopb.RegisterTodoServiceServer(s, grpc_server.NewGRPCServer(svc))

	if er = s.Serve(lis); er != nil {
		log.Fatalf("%v", er)
	}
}

// POST /todo => create a todo for a user
// DELETE /todo/{id} => delete a todo by id
// PUT /todo/{id}/status => set status (done: true/false)
// GET /todo/{id} => get singe todo by id

// GET /todos/{user-id} get user's todos
