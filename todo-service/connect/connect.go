package connect

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"todo-service/config"

	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/postgres"
	_ "github.com/golang-migrate/migrate/source/file"
	_ "github.com/lib/pq"
)

func Connect(cfg config.Config) (*sql.DB, error) {
	db, er := sql.Open("postgres",
		fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
			cfg.PostgresHost, cfg.PostgresPort, cfg.PostgresUser, cfg.PostgresPassword, cfg.PostgresDb))
	if er != nil {
		return nil, er
	}

	if er = db.Ping(); er != nil {
		return nil, er
	}

		driver, er := postgres.WithInstance(db, &postgres.Config{})
		if er != nil {
			return nil, er
		}
		m, er := migrate.NewWithDatabaseInstance(
			"file://migrations",
			"postgres", driver,
		)
		if er != nil {
			return nil, er
		}
		if er = m.Up(); er != nil && !errors.Is(er, migrate.ErrNoChange) {
			return nil, fmt.Errorf("failed to migrate: %v", er)
		}
	return db, nil
}
