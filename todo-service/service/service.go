package service

import (
	"context"
	"errors"
	"log"
	"todo-service/psql"
	"todo-service/todo"
)

type ServiceRepo interface {
	CreateTodo(ctx context.Context, body, userId string) (string, error)
	DeleteTodo(ctx context.Context, todoId string) error
	UpdateTodo(ctx context.Context, todo todo.Todo) (string, error)
	GetTodo(ctx context.Context, todoId string) (todo.Todo, error)
	GetTodosByUser(ctx context.Context, userId string) ([]todo.Todo, error)
}

type Service struct {
	psql psql.PsqlRepo
}

func New(psql psql.PsqlRepo) Service {
	return Service{
		psql: psql,
	}
}

func (s Service) GetTodo(ctx context.Context, todoId string) (todo.Todo, error) {
	t, er := s.psql.GetTodo(ctx, todoId)
	if er != nil {
		return todo.Todo{}, er
	}
	return t, nil
}

func (s Service) UpdateTodo(ctx context.Context, todo todo.Todo) (string, error) {
	todoId, er := s.psql.UpdateTodo(ctx, todo)
	if er != nil {
		return "", er
	} else if er == errors.New("could not find todo") {
		return "", er
	}
	return todoId, nil
}

func (s Service) GetTodosByUser(ctx context.Context, userId string) ([]todo.Todo, error) {
	ts, er := s.psql.GetTodosByUser(ctx, userId)
	if er != nil {
		return nil, er
	}
	return ts, nil
}

func (s Service) DeleteTodo(ctx context.Context, todoId string) error {
	if er := s.psql.DeleteTodo(ctx, todoId); er != nil {
		return er
	}
	return nil
}

func (s Service) CreateTodo(ctx context.Context, body, userId string) (string, error) {
	todo := todo.New(body, userId)
	if er := s.psql.InsertTodo(ctx, todo); er != nil {
		return "", er
	}
	return todo.Id, nil
}
